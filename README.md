Выполнил: Цзинь Мин Чэнь Гуаньюань
Группа: P3304C
Лабораторнаяработа1
Название： “Разработкадрайверовсимвольныхустройств”
Цельработы：При записи текста в файл символьного устройства должен осуществляться подсчет введенных символов. Последовательность полученных результатов (количество символов) с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.
При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных, которые выводятся при чтении файла /proc/varN.

Описаниефункциональностидрайвера
При записи текста в файл символьного устройства должен осуществляться подсчет введенных символов. Последовательность полученных результатов (количество символов) с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.
При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных, которые выводятся при чтении файла /proc/varN.
Инструкцияпосборке
In the terminal, use the "make" command to load the program, run sudo insmod proc_example1.ko to load the module, and run sudo mknod /dev/var1 c xxx 0 to create a character device.Инструкцияпользователя
test2.sh and test3.sh are scripts used for testing, run with the sudo bash ./test2.sh command.

test2.sh and test3.sh are used to write data to the /proc/var1 and /dev/var1 devices, respectively.

After running, the program will count the length of input characters. The script can be run multiple times, and the kernel module will record the number of characters each time.

Running sudo cat /proc/var1 or sudo cat /dev/var1 will output the same sequence of characters.

After the test is completed, use sudo rmmod proc_example1.ko to uninstall the module,
Use sudo rm /dev/var1 to delete the character device just created.
Примерыиспользования

